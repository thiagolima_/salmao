import { defineConfig, resolveEnvPrefix } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    server: {
        proxy: {
            '/produtos-0.0.1': {
                target: 'http://68.183.24.34:8080',
                secure: false,
                changeOrigin: true,
            },
            '/autenticador/oauth/token': {
                target: 'http://68.183.24.34:8080',
                secure: false,
                changeOrigin: true,
            },
            '/api-pagamento': {
                target: 'http://159.203.181.176:8085',
                secure: false,
                changeOrigin: true,
            },
        },
    },
})
