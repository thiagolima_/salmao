/** @type {import('tailwindcss').Config} */
export default {
    content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    theme: {
        extend: {
            extend: {
                fontFamily: {
                    roboto: ['Roboto', 'sans-serif'],
                },
            },
        },
    },
    daisyui: {
        themes: [
            {
                mytheme: {
                    primary: '#00003D',
                    secondary: '#4E4EC4',
                    accent: '#1dcdbc',
                    neutral: '#2b3440',
                    'base-100': '#ffffff',
                    info: '#3abff8',
                    success: '#36d399',
                    warning: '#DB9B35',
                    error: '#f87272',
                },
            },
        ],
    },
    plugins: [require('daisyui')],
}
