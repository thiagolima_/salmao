import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from '../stores'

const index = () => import(/* webpackChunkName: "index" */ '../pages/index.vue')

const checkout = () =>
    import(/* webpackChunkName: "index" */ '../pages/checkout.vue')

const login = () =>
    import(/* webpackChunkName: "index" */ '../pages/auth/login.vue')

const carrinho = () =>
    import(/* webpackChunkName: "index" */ '../pages/carrinho.vue')

const registro = () =>
    import(/* webpackChunkName: "index" */ '../pages/auth/registro.vue')

const router = createRouter({
    routes: [
        { path: '/', component: index },
        { path: '/checkout', component: checkout },
        { path: '/login', component: login },
        { path: '/carrinho', component: carrinho },
        { path: '/registro', component: registro },
    ],
    history: createWebHistory(),
})

router.beforeEach((to, _, next) => {
    const { isAuthenticated } = useAuthStore()

    if (
        (to.path === '/checkout' && !isAuthenticated) ||
        (to.path === '/login' && isAuthenticated)
    ) {
        next('/')
    } else {
        next()
    }
})

export default router
