import { defineStore } from 'pinia'
import { Content as ReponseTypeProduct } from '../services'
import { useLocalStorage } from '../composables'

export const useCartStore = defineStore('cart', {
    state: () => {
        const { storedValue } = useLocalStorage(
            'cart',
            [] as ReponseTypeProduct[]
        )

        return {
            items:
                (storedValue as ReponseTypeProduct[]) ||
                ([] as ReponseTypeProduct[]),
        }
    },
    getters: {
        totalItems: (state) => state.items.length,
        subtotal: (state) =>
            state.items.reduce(
                (accumulator, currentValue) =>
                    accumulator + currentValue.valor * currentValue.qtd!,
                0
            ),
    },
    actions: {
        addToCart(item: ReponseTypeProduct) {
            const { setStoredValue } = useLocalStorage(
                'cart',
                [] as ReponseTypeProduct[]
            )
            const products = { ...item, qtd: 1 }
            this.items = [
                ...this.items.filter((product) => item.id !== product.id),
                products,
            ]
            setStoredValue(this.items)
        },
        removeFromCart(productId: number) {
            const { setStoredValue } = useLocalStorage(
                'cart',
                [] as ReponseTypeProduct[]
            )

            this.items = this.items?.filter((item) => item.id !== productId)
            setStoredValue(this.items)
        },
        chagenQtd(productId: number, quantity: number = 1) {
            const { setStoredValue } = useLocalStorage(
                'cart',
                [] as ReponseTypeProduct[]
            )

            this.items = this.items.map((item) => {
                if (item.id === productId) {
                    return { ...item, qtd: item.qtd! + quantity }
                } else {
                    return item
                }
            })
            setStoredValue(this.items)
        },
        cleanCart() {
            this.items = []
            const { setStoredValue } = useLocalStorage(
                'cart',
                [] as ReponseTypeProduct[]
            )
            setStoredValue([])
        },
    },
})
