import { defineStore } from 'pinia'
import { useLocalStorage } from '../composables'

export const useAuthStore = defineStore('atuth', {
    state: () => ({ user: {} as { cpf: string; nome: string } }),
    getters: {
        isAuthenticated: (state) => {
            const { storedValue } = useLocalStorage('usuario4455', '')
            return state.user || storedValue.value
        },
    },
    actions: {
        setUser(user: any) {
            this.user = user
        },
    },
})
