import { useForm } from 'vee-validate'
import { toTypedSchema } from '@vee-validate/zod'
import { z } from 'zod'

const required = { required_error: 'Campo obrigatório' }

const schema = z.object({
    nome: z.string(required).nullish(),
    cpf: z.string(required).nullish(),
    cnpj: z.string(required).nullish(),
    inscricaoEstadual: z.string(required).nullish(),
    razaoSocial: z.string(required).nullish(),
    senha: z.string(required).min(6, { message: 'Mínimo 6 caracteres' }),
    confirmarSenha: z
        .string(required)
        .min(6, { message: 'Mínimo 6 caracteres' }),
    email: z.string(required).email({ message: 'Insira um email válido' }),
    telefone: z.string().nullish(),
    estado: z.string(required),
    cidade: z.string(required),
    enderecoClienteFormDto: z.object({
        cep: z.string(required),
        logradouro: z.string(required),
        complemento: z.string(required),
        bairro: z.string(required),
    }),
})

export type SchemaRegistro = z.infer<typeof schema>

export function useRegistro(data?: SchemaRegistro) {
    const validationSchema = toTypedSchema(schema)

    const form = useForm<SchemaRegistro>({
        validationSchema,
        initialValues: data,
    })

    return {
        validationSchema,
        ...form,
    }
}
