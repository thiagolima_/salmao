import { useForm } from 'vee-validate'
import { toTypedSchema } from '@vee-validate/zod'
import { z } from 'zod'

const schema = z.object({
    email: z
        .string({ required_error: 'Campo obrigatório' })
        .email({ message: 'Insira um email válido' }),
    senha: z
        .string({ required_error: 'Campo obrigatório' })
        .min(6, { message: 'Senha curta' }),
})

type SchemaLogin = z.infer<typeof schema>

export function useLogin(data?: SchemaLogin) {
    const validationSchema = toTypedSchema(schema)

    const form = useForm<SchemaLogin>({
        validationSchema,
        initialValues: data,
    })

    return {
        validationSchema,
        ...form,
    }
}
