import { ref, onMounted } from 'vue'

// Define a composable function
export function useLocalStorage<T>(key: string, initialValue: T) {
    // Get the initial value from local storage or use the provided initial value
    const storedValue = ref(
        localStorage.getItem(key)
            ? JSON.parse(localStorage.getItem(key)!)
            : initialValue
    )

    // Watch for changes to the stored value and update local storage accordingly
    function setStoredValue(value: T) {
        storedValue.value = value
        localStorage.setItem(key, JSON.stringify(value))
    }

    // Watch for changes to the key and update the stored value accordingly
    function setKey(newKey: string) {
        const currentValue = storedValue.value
        localStorage.removeItem(key)
        localStorage.setItem(newKey, JSON.stringify(currentValue))
        key = newKey
    }

    // On component mount, initialize the stored value
    onMounted(() => {
        if (!localStorage.getItem(key)) {
            localStorage.setItem(key, JSON.stringify(initialValue))
        }
    })

    return {
        storedValue,
        setStoredValue,
        setKey,
    }
}
