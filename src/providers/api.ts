import axios from 'axios'

export const api = axios.create({
    baseURL: 'http://68.183.24.34:8080/produtos-0.0.1',
})

export const auth = axios.create({
    baseURL: '/autenticador/oauth/token',
    headers: {
        Authorization: 'Basic MzpYbmMwTTBvam5DVCQmTnRrI0owXjdUeVl1MVlGQjI=',
        'Content-Type': 'application/x-www-form-urlencoded',
    },
})

export const pagamento = axios.create({
    baseURL: 'http://159.203.181.176:8085/api-pagamento',
})
