export interface Carrinho {
    content: Content[]
    empty: boolean
    first: boolean
    last: boolean
    number: number
    numberOfElements: number
    pageable: Pageable
    size: number
    sort: Sort2
    totalElements: number
    totalPages: number
}

export interface Content {
    estoqueMinimo: number
    id: number
    idLojista: number
    produto: Produto
    valor: number
    qtd?: number
}

export interface Produto {
    alturaCm: number
    categoria: Categoria
    cnp: string
    descricao: string
    detalhe: string
    id: number
    imagens: Imagen[]
    larguraCm: number
    manualInstrucao: string
    modelo: Modelo
    ncm: string
    pesoGrama: number
    produtoLiberado: boolean
    profundidadeCm: number
    quantidadeApresentacao: number
    sku: string
    unidadeMedidaProduto: UnidadeMedidaProduto
    videoDemonstrativo: string
}

export interface Categoria {
    departamento: Departamento
    descricao: string
    id: number
}

export interface Departamento {
    descricao: string
    id: number
}

export interface Imagen {
    arquivo: string
    ativo: boolean
    cor: Cor
    dataCadastro: string
    dataExclusao: string
    extensao: string
    id: number
    nome: string
    observacao: string
    tamanho: number
}

export interface Cor {
    descricao: string
    id: number
}

export interface Modelo {
    descricao: string
    id: number
    marca: Marca
}

export interface Marca {
    descricao: string
    id: number
}

export interface UnidadeMedidaProduto {
    descricao: string
    id: number
    sigla: string
}

export interface Pageable {
    offset: number
    pageNumber: number
    pageSize: number
    paged: boolean
    sort: Sort
    unpaged: boolean
}

export interface Sort {
    empty: boolean
    sorted: boolean
    unsorted: boolean
}

export interface Sort2 {
    empty: boolean
    sorted: boolean
    unsorted: boolean
}
