import { createApp } from 'vue'
import './style.css'
import { OhVueIcon, addIcons } from 'oh-vue-icons'
import {
    HiSolidCreditCard,
    HiShoppingCart,
    HiUser,
    MdPix,
    HiSolidTrash,
    HiHeart,
} from 'oh-vue-icons/icons'
import router from './router'
import App from './App.vue'
import { createPinia } from 'pinia'
import { VueQueryPlugin } from '@tanstack/vue-query'

addIcons(
    HiShoppingCart,
    HiUser,
    HiHeart,
    HiSolidCreditCard,
    MdPix,
    HiSolidTrash
)
const pinia = createPinia()

const app = createApp(App)
app.use(router)
app.use(pinia)
app.use(VueQueryPlugin)
app.component('v-icon', OhVueIcon)
app.mount('#app')
